import "jquery-mask-plugin";
import $ from 'jquery';

import landingPageModular from './scripts/landingPageModular';
import termosTextosLegais from './scripts/termosTextosLegais';
import institucional from './scripts/institucional';
import contato from './scripts/contato';
import faq from './scripts/faq';
import magazineTodas from './scripts/magazine-todas';
import home from './scripts/home';
import produto from './scripts/produto';
import login from "./scripts/login";
import vitrine from "./scripts/vitrine";
import voltarTopo from "./scripts/voltarTopo";
import { NewsletterHome } from "./scripts/newsletter2";
import adicionarGravacao from "./scripts/adicionarGravacao";



adicionarGravacao();
NewsletterHome();
login();
voltarTopo();
landingPageModular();
termosTextosLegais();
institucional();
contato();
faq();
magazineTodas();
home();
produto();

// PLAYER VIDEO
function playVideo() {
  let video = $(".player-video");

  video.on("click", function () {
    let id = $(this).attr('id');
    let iframe = "#" + id + " iframe";
    $(iframe).attr('src', $(iframe).attr('src') + '?autoplay=1');
    setTimeout(function () { $("#" + id).addClass("player"); }, 500);
  });
};
playVideo();
vitrine();
