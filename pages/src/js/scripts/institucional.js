import $ from 'jquery';

function institucional() {
    $('.institucional-titles .title').click(function () {
        var id = $(this).attr('id');
        $('.title').removeClass('active');
        $('#' + id).addClass('active');
        $('.institucional-contents .text').removeClass('active');
        $('.institucional-contents #' + id).addClass('active');
    })

    $('.institucional-titles .title').on('click', function (e) {
        e.preventDefault();

        const tela = $(window).width();

        if (tela <= 950) {
            const targetOffset = $('.institucional-contents').offset().top;

            $('html, body').animate({
                scrollTop: targetOffset - 200
            }, 500);
        }

    });
}

export default institucional;
