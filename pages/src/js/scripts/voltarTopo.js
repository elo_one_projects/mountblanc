import $ from 'jquery';

function voltarTopo() {

    const div = $('.footer-website .v-topo');

    div.click(function () {
        $('html, body').animate({ scrollTop: 0 }, 'slow');
    })
}

export default voltarTopo;
