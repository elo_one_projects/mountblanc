import $ from 'jquery';

function vitrine() {
  $('.box-links-navigator').prepend('<span class="voltar">Voltar</span>')
  $('.box-links-navigator').prepend('<i class="lni-arrow-left voltar"></i>')

  $('.searchResultsTime .resultado-busca-numero .label').text('Produtos');

  $('.searchResultsTime .resultado-busca-numero .label').appendTo

  $('.resultado-busca-filtro .orderBy select option').first().text('');

  $('.resultado-busca-filtro .orderBy').click(function () {
    $(this).toggleClass('active');
  })

  $(".box-links-navigator .voltar").click(function () {
    window.history.back();
  })

  $('.search-multiple-navigator h3').appendTo('.box-links-navigator');
  $('.search-multiple-navigator h4').appendTo('.box-links-navigator');

  // $('body.vitrines .box.filtro .menu-departamento .search-multiple-navigator fieldset')

  const boxSearchResultsTime = 'section.box.filtro .content .box-searchResultsTime';
  $('p.searchResultsTime').appendTo(boxSearchResultsTime);
  $('.main .sub').appendTo(boxSearchResultsTime);
  $(boxSearchResultsTime).append('<div class="btn-mostrar-filtros">Mostrar todos os filtros</div>');
  $(boxSearchResultsTime + ' .btn-mostrar-filtros').click(function () {
    $(this).toggleClass('active');
    const text = $(this).text();
    $('.search-multiple-navigator fieldset').slideToggle();

    if (text == 'Mostrar todos os filtros') {
      $(this).text('Mostrar menos filtros');
    } else {
      $(this).text('Mostrar todos os filtros');
    }
  })

  $('.search-multiple-navigator fieldset').map(function (x) {
    const classe = x.toString();
    $(this).addClass(classe);
  })


  $('.search-multiple-navigator fieldset').click(function (e) {
    const id = $(this).attr('class').split(' ').join('.');

    $('.search-multiple-navigator .' + id + ' h5').toggleClass('active');
    $('.search-multiple-navigator .' + id + ' div').slideToggle();
  })

  let aux;

  $(window).scroll(function () {
    let top = $(this).scrollTop();

    if (top > aux) {
      $('body.vitrines .box.filtro').css('padding-top', '60px');
    }

    if (top < aux) {
      $('body.vitrines .box.filtro').css('padding-top', '130px');
    }

    aux = top;
  });

  const qtdFiledset = $('.search-multiple-navigator fieldset').length;
  if (qtdFiledset < 5) {
    $('.box-searchResultsTime .btn-mostrar-filtros').hide();
  }
}

export default vitrine;
