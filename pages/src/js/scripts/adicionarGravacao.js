import $ from 'jquery';

function adicionarGravavao() {

  const pagina = document.querySelector('body').classList;

  console.log(pagina[0]);
  if (pagina[0] === 'produtos') {
    const btnPersonalizar = document.querySelector('#btn-add-personalizar .buy-in-page-button');
    const posicaoInput = document.querySelectorAll('#modal-personalizar input[name="posicao"]');
    const tipografiaInput = document.querySelectorAll('#escolha-tipografia input[name="topigrafia"]');
    const textoInput = document.querySelector('#modal-personalizar #texto-gravacao');

    const confirmText = $('#modal-personalizar .confirmText');

    const gravacao = {
      posicao: 'tampa',
      tipografia: 'gravacao01',
      texto: '',
    };

    posicaoInput.forEach(item => {
      item.addEventListener('click', function () {
        gravacao.posicao = this.value;
      });
    })

    tipografiaInput.forEach(item => {
      item.addEventListener('click', function () {
        gravacao.tipografia = this.value;
      });
    })

    textoInput.addEventListener('click', function () {
      confirmText.text('');
    })

    console.log(gravacao.texto.length);

    btnPersonalizar.addEventListener('click', function (e) {
      gravacao.texto = textoInput.value;

      if (gravacao.texto.length == 0) {
        e.preventDefault();
        confirmText.text('Escreva o seu texto!');
      } else {
        let productId;
        vtexjs.catalog.getCurrentProductWithVariations().done(function (product) {
          productId = product.productId;
        });

        // const productId = 1;

        sessionStorage.setItem(productId, JSON.stringify(gravacao));

        $("#modal-personalizar").css('display', 'none');
        $('input#texto-gravacao').val('');
        $('#valorGravacao').text('');

        window.location.reload();
      }
    })
  }

  // **************** CÓDIGO DO CHECKOUT *********************************************************
  // vtexjs.checkout.orderForm.items.forEach(function (item, index) {
  //   const sessionObject = JSON.parse(sessionStorage.getItem(item.id));
  //
  //   function gravuraDisparo(sessionObject) {
  //     var itemIndex = index;
  //     var attachmentName = "gravura";
  //     var content = {
  //       "gravura": JSON.stringify({
  //         gravura: sessionObject
  //       })
  //     };
  //     vtexjs.checkout.addItemAttachment(itemIndex, "gravura", content).done(function (res) {
  //       console.log(res)
  //     })
  //   }
  //   gravuraDisparo(sessionObject);
  // });
}

export default adicionarGravavao;
