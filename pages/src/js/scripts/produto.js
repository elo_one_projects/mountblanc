import $ from 'jquery';
import 'slick-carousel';

function produto() {
  $('.body-product .apresentacao .thumbs').not('.slick-initialized').slick({
    infinite: true,
    speed: 600,
    slidesToShow: 1,
    slidesToScroll: 1,
    adaptiveHeight: true,
    dots: true,
    autoplay: false,
    prevArrow: `<div class='arrow prev'><i class="lni-arrow-left"></i></div>`,
    nextArrow: `<div class='arrow next'><i class="lni-arrow-right"></i></div>`
  });

  $('.linha.btn-comprar a').text('COMPRE AGORA');

  $(".body-product .lista-desejos").click(function () {
    $(".body-product .lista-desejos .icon-wish").toggleClass("active");
  })

  $(".produto-item .links .voltar").click(function () {
    window.history.back();
  })

  let skuBestPrice = $('.body-product .skuBestPrice').text();
  let DivBestPrice = $('.body-product .price-best-price');
  DivBestPrice.text(skuBestPrice + " à vista");
  // $(".body-product .valor-dividido .skuBestInstallmentValue").append(" com juros");

  let qtdThumbs = $(".apresentacao .thumbs ul li").length;
  let qtdSlide = $('.body-product .qtd-slide');
  qtdSlide.html("1/" + qtdThumbs);

  $('.apresentacao .thumbs').on('afterChange', function (event, slick, currentSlide) {
    let slide = currentSlide + 1;
    qtdSlide.html(slide + "/" + qtdThumbs);
  });

  $(".produto-descricao .links .link").click(function () {
    let id = $(this).attr('id');
    $(".produto-descricao .links .link").removeClass('active');
    $(".produto-descricao .links #" + id).addClass('active');
    $(".produto-descricao .boxs .box-conteudo").removeClass('active');
    $(".produto-descricao .boxs .box-conteudo .texto").removeClass('active');
    $(".produto-descricao .boxs #" + id).addClass('active');
    $(".produto-descricao .boxs .box-conteudo .btn-lerMais").remove();
    $(".produto-descricao .boxs #" + id + ' .fa-chevron-down').removeClass('active');
    $(".produto-descricao .boxs #" + id + ' .fa-chevron-up').addClass('active');

    let qtdLinhas = $(".produto-descricao .boxs #" + id).text().length;
    if (qtdLinhas > 749) {
      $(".produto-descricao .boxs #" + id).append(`
                <div class="btn-lerMais">
                    <p>Ler Mais</p>
                    <i class='fas fa-chevron-down'></i>
                    <i class='fas fa-chevron-up active'></i>
                </div>
            `)
    }
    $(".produto-descricao .boxs #" + id + " .btn-lerMais").click(function () {
      $(".produto-descricao .boxs #" + id + ' .fa-chevron-up').toggleClass('active');
      $(".produto-descricao .boxs #" + id + ' .fa-chevron-down').toggleClass('active');
      $(".produto-descricao .boxs #" + id + " .texto").addClass("active", 1000);
      $(this).css("display", "none");
    })
  })

  $('#btn-add-personalizar .buy-in-page-button').text('ADICIONAR À SACOLA');

  let $classe = $('.banner-product2-produto .products ul');
  $classe.find('.helperComplement').remove();
  $classe.not('.slick-initialized').slick({
    infinite: true,
    speed: 600,
    slidesToShow: 3,
    slidesToScroll: 1,
    dots: false,
    autoplay: false,
    prevArrow: `<div class='arrow prev'><i class="lni-arrow-left"></i></div>`,
    nextArrow: `<div class='arrow next'><i class="lni-arrow-right"></i></div>`,
    responsive: [
      {
        breakpoint: 950,
        settings: {
          slidesToShow: 1
        }
      }
    ]
  });

  // MODAL COMPRA
  setTimeout(function () {
    const modal1 = $("#modal-add-produto");
    const modal2 = $("#modal-personalizar");
    const btn1 = $("#btn-compre-agora .buy-in-page-button");
    const btn2 = $("#btn-personalize");
    const btnClose1 = $("#btn-close-modal-1");
    const btnClose2 = $("#btn-close-modal-2");
    const btnCancel2 = $("#btn-cancel-personalizar");

    btn1.click(function () {
      $(modal1).css('display', 'block');
    })
    btnClose1.click(function () {
      $(modal1).css('display', 'none');
      window.location.reload();
    })
    btn2.click(function () {
      $(modal2).css('display', 'block');
    })

    function modalTextInput() {
      $(modal2).css('display', 'none');
      $('input#texto-gravacao').val('');
      $('#valorGravacao').text('');
      $('#modal-personalizar .confirmText').text('');
    }

    btnClose2.click(function () {
      modalTextInput();
    })
    btnCancel2.click(function () {
      modalTextInput();
    })
  }, 50);
  let skuBestPriceModal = $('#modal-personalizar .skuBestPrice').text();
  let DivBestPriceModal = $('#modal-personalizar .price-best-price');
  DivBestPriceModal.text(skuBestPriceModal + " à vista");

  window.alert = function () { };

  const btnSacola = $('#modal-add-produto .text-topic .btn-sacola a');
  btnSacola.text('ADICIONAR À SACOLA');

  $('#modal-add-produto .text-topic .nome h2').click(function () {
    const id = $(this).attr('class');
    $('#modal-add-produto #' + id + ' .conteudo').slideToggle();
    $('#modal-add-produto #' + id + ' .conteudo .btn-sacola a').click(function () {
      $('#modal-add-produto #' + id + ' .conteudo').slideUp();
      $(this).text('PRODUTO ADICIONADO À SACOLA');
    })
  })


  function clickPosicaoGravacao(posicao, imgTrue, imgFalse, maxlength, local) {
    $(posicao).click(function () {
      const gravImg = $(imgTrue);
      const gravImgTampa = $(imgFalse)
      const valorGravacao = $('#valorGravacao');

      gravImg.css('display', 'block');
      gravImgTampa.css('display', 'none');
      valorGravacao.removeClass('clip');
      valorGravacao.addClass(local);

      $('#valorGravacao').text('');
      $('input#texto-gravacao').val('');
      $('input#texto-gravacao').attr('maxlength', maxlength);
    })
  }

  $('#escolha-tipografia .grav-input input').click(function () {
    const id = $(this).attr('id');
    const valorGravacao = $('#valorGravacao');
    valorGravacao.removeClass('fonte01');
    valorGravacao.removeClass('fonte02');
    valorGravacao.removeClass('fonte03');
    valorGravacao.addClass(id);
  })

  // Tampa da caneta
  clickPosicaoGravacao('.grav-input #tampa', '#grav-img', '#grav-img-clip', 13, null);

  // Clip da caneta
  clickPosicaoGravacao('.grav-input #clip', '#grav-img-clip', '#grav-img', 3, 'clip');

  $('input#texto-gravacao').on('keyup', function () {
    $('#valorGravacao').text(($(this).val()));
  })
}

export default produto;
