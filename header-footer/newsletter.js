import $ from "jquery";
import Swal from "sweetalert2";
export function Newsletter() {
  $("footer .newsletter .box.news-inputs .button").click(function() {
    console.log("news");
    var cliente = {
      nome: $("input#name").val(),
      email: $("input#email").val()
    };
    $.ajax({
      type: "POST",
      url: window.location.origin + "/api/dataentities/NL/documents",
      processData: false,
      data: JSON.stringify(cliente),
      cache: false,
      async: true,
      crossDomain: true,
      headers: {
        "Content-Type": "application/json",
        Accept: "application/vnd.vtex.ds.v10+json"
      },
      success: function(result) {
        $("input")
          .empty()
          .val("");
        Swal.fire({
          title: "Obrigado!",
          type: "success",
          text: "Em breve você ira receber novidades, promoções e muito mais",
          animation: false,
          timer: 3000
        });
      },
      error: function(result) {
        Swal.fire({
          type: "error",
          title: "Algo deu errado",
          text: result.responseText.split(":")[1].split("}")[0],
          animation: false
        });
      }
    });
  });
}

export function Contato() {
  $(".box.formulario-contato .botao-enviar").click(function() {
    console.log("Contato");
    var cliente = {
      nome: $('.box.formulario-contato input[type="text"]:eq(0)').val(),
      email: $('.box.formulario-contato input[type="text"]:eq(1)').val(),
      assunto: $("select#assunto").val(),
      mensagem: $("textarea#mensagem").val()
    };
    $.ajax({
      type: "POST",
      url: window.location.origin + "/api/dataentities/CO/documents",
      processData: false,
      data: JSON.stringify(cliente),
      cache: false,
      async: true,
      crossDomain: true,
      headers: {
        "Content-Type": "application/json",
        Accept: "application/vnd.vtex.ds.v10+json"
      },
      success: function(result) {
        $("input")
          .empty()
          .val("");
        Swal.fire({
          title: "Obrigado!",
          type: "success",
          text:
            "Sua solicitação foi processada com sucesso. Em breve, nossa equipe entrará em contato!",
          animation: false,
          timer: 4000
        });
      },
      error: function(result) {
        Swal.fire({
          type: "error",
          title: "Algo deu errado",
          text: result.responseText.split(":")[1].split("}")[0],
          animation: false
        });
      }
    });
  });
}
