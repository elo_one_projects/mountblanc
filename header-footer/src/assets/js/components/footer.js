import $ from 'jquery';

function Footer() {

    const div = $('.copyright .v-topo');

    div.click(function () {
        $('html, body').animate({ scrollTop: 0 }, 'slow');
    })

    $('footer').prepend(`
    <section class="precisa-ajuda">
       Precisa de ajuda? Por favor entre em contato através do telefone 1-800-995-4810
    </section>
    `);
}

export default Footer;