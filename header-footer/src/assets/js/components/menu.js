import $ from "jquery";

export default function Menu() {
  function login() {
    $.getJSON(window.location.origin + '/no-cache/profileSystem/getProfile', function (response) {
      if (response.IsUserDefined == true) {
        $('header .icon.login').hide();
        $('header .icon.logout').show();
      }
    });
  }

  login();


  $('header .icon.minicart').click(function () {
    const larguraTela = $(window).width();

    if (larguraTela < 951) {
      window.location.href = window.location.origin + "/checkout/#/cart";
    }
  })

  $('button.menu-mobile').click(function () {
    $(this).toggleClass('active');
    $('header .menu').slideToggle();
  })

  let num;

  const bodyClass = $('body').attr('class');

  if (bodyClass !== 'home') {
    $('header').addClass('actived');
  }

  $(window).scroll(function () {
    let top = $(this).scrollTop();

    if (top > num) {
      $('section.top-news').slideUp();
      $('.header-web-site .content').slideUp();
      $('header section.header-web-site .content').removeClass('up');
    }

    if (top < num) {
      $('.header-web-site .content').slideDown();
      $('header section.header-web-site .content').addClass('up');
    }

    if (top > 1) {
      $('header').addClass('actived');
    } else {
      if (bodyClass === 'home') {
        $('header').removeClass('actived');
      }
    }

    num = top;
  });


  $('section.top-news svg').click(function () {
    $('section.top-news').slideUp();
  })

  $('.search .search-icon').click(function () {
    $('header .menu-busca').slideToggle();
  });

  $('header .search-title .btn-close-busca').click(function () {
    $('header .menu-busca').slideUp();
  });

  $('.fulltext-search-box').click(function () {
    $('ul.ui-autocomplete').appendTo('header .menu-busca');
  });

  // MINICART
  $('.minicart').mouseover(function () {
    const btnFinalizar = $('.cartFooter .cartCheckout');
    $('.portal-minicart-ref').css('display', 'block');
    $('.vtexsc-productList thead .cartSkuQuantity').text('QTDE');
    $('.cartSkuRemove a').show().text('x');
    btnFinalizar.text('FINALIZAR COMPRA');
  });
  $('.portal-minicart-ref' || '.v2-vtexsc-cart').mouseover(function () {
    const qtd = $('.vtexsc-productList tbody tr').length;
    $('.qtd-cart').text(qtd);
    if (qtd < 1) {
      $('.minicart').mouseover(function () {
        $('.minicart .minicart-vazio').show();

        $(this).mouseleave(function () {
          $('.minicart .minicart-vazio').hide();
        })
      });
      $('.qtd-cart').text('');
    } else {
      $('.minicart .minicart-vazio').hide();
    }
  });
  vtexjs.checkout.getOrderForm().done(function (orderForm) {
    const qtd = orderForm.items.length;
    if (qtd < 1) {
      $('.qtd-cart').text('');
    } else {
      $('.qtd-cart').text(qtd);
    }
  });
};