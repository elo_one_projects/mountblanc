import $ from "jquery";
export default function BannerHome(classBody, classElement) {
  if ($(classBody).length > 0) {
    $(classElement).slick({
      dots: true,
      infinite: false,
      speed: 600,
      slidesToShow: 1,
      adaptiveHeight: true,
      autoplay: true,
      autoplaySpeed: 5000,
      arrows: true,
      nextArrow: `<svg class="direita" width="36" height="14" viewBox="0 0 36 14" fill="none" xmlns="http://www.w3.org/2000/svg">
      <path d="M1 7H35" stroke="white" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"/>
      <path d="M29 13L35 7L29 1" stroke="white" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"/>
      </svg>`,
      prevArrow: `<svg class="esquerda" width="36" height="14" viewBox="0 0 36 14" fill="none" xmlns="http://www.w3.org/2000/svg">
      <path d="M35 7H1" stroke="white" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"/>
      <path d="M7 13L1 7L7 1" stroke="white" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"/>
      </svg>
      `
    });
  }
}
