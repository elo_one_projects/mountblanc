import $ from "jquery";
import Swal from "sweetalert2";
export function Newsletter() {
  $("footer .newsletter label svg").click(function () {
    console.log("news");
    var cliente = {
      email: $("footer .newsletter label input").val(),
    };

    $.ajax({
      type: "POST",
      url: window.location.origin + "/api/dataentities/NL/documents",
      processData: false,
      data: JSON.stringify(cliente),
      cache: false,
      async: true,
      crossDomain: true,
      headers: {
        "Content-Type": "application/json",
        Accept: "application/vnd.vtex.ds.v10+json"
      },
      success: function (result) {
        $("input")
          .empty()
          .val("");
        Swal.fire({
          title: "Obrigado!",
          type: "success",
          text: "Em breve você ira receber novidades, promoções e muito mais",
          animation: false,
          timer: 3000
        });
      },
      error: function (result) {
        Swal.fire({
          type: "error",
          title: "Algo deu errado",
          text: result.responseText.split(":")[1].split("}")[0],
          animation: false
        });
      }
    });
  });
}